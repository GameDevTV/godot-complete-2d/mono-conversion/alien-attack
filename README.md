# Alien Attack in Mono

Translation of the Alien Attack GDScript into C#, to be used with Godot Mono.

## Installation

You can zip (by downloading) and unzip the project in any directory you want or clone the repository, open it using Godot.
The code is written and tested with: 
- Godot 4.1.1 Mono, 
- Godot 4.1.2 Mono, 
- Godot 4.2 - Beta 1 Mono,  
- Godot 4.2 - Beta 2 Mono & 
- Godot 4.2 - Beta 3 Mono. 

__Note and personal disclaimer__:  
 The naming standard as normally used in C# is not fully applied, this is to accommodate students  
 who follow the course to see as close as possible the same names for variables, methods, signals and classes.
 Noticeable differences are:
  - Prefix with underscore _ means internal class variable, mostly also used for an additional variable loading the scenes and/or referencing the class node or variables that are needed next to the specified course. Sometimes the _ is not there as it's using the template code (like Velocity velocity;)
  - Class names are mostly starting in lower case same as during the course in GDScrit
  - Signal are using a Godot API convention having underscores but EventHandler() as sufficient, after a project build these will show up in the Godot Inspector
  - Methods are named on how they were used in the course so you will find a mix of Snake Case (first_name), Pascal Case (FirstName) and Camel Case (firstName) variations.
  - I added some comments when it is not evident how to translate it in a one to one fashion from GDScript to C#.

As pointed out this is to stay as close to the course material as possible.  
__I do strongly advice__ when you program by you own to follow the normal guidelines where possibe:
 -   [C# identifier names]https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/identifier-names
 -   [C# code conventions]https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions
And the advice of Godot on the C#:
 -   [Godot Engine documentation Scripting C#]https://docs.godotengine.org/en/stable/tutorials/scripting/c_sharp/index.html

## Usage

Usage is alongside the course delivered by GameDev.tv (the course is not supplied by me):  
[Complete Godot 2D: Develop Your Own 2D Games Using Godot 4]https://www.gamedev.tv/p/godot-complete-2d


## Contributing

This project is intended as a contribution to GameDev.tv to demonstrate the replacement of GDScript with C#.


## License

[MIT](https://choosealicense.com/licenses/mit/)
